# Mathezirkel Webserver Helm chart

![Version: 7.0.1](https://img.shields.io/badge/Version-7.0.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy a webserver maintained in [this repository](https://gitlab.com/mathezirkel/server/website) on Kubernetes.

The chart requires [Traefik v2](https://traefik.io/traefik/) with its associated CRDs
and [Argo Workflows](https://argoproj.github.io/workflows/) along with its CRDs if backups are enabled.

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://gitlab.com/api/v4/projects/50479089/packages/helm/main/ | redis | 1.1.1 |

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace webserver
    ```

2. Create a secret for the redis credentials:

    ```shell
    kubectl create secret generic redis-credentials \
    --from-literal=password=CHANGEME \
    -n webserver
    ```

    **Note**: The sign '@' is not allowed in the password string.

3. Create a secret for the django secret key.

    ```shell
    kubectl create secret generic django-secret-key \
    --from-literal=secret=CHANGEME \
    -n webserver
    ```

4. Create a secret for mail credentials.

    ```shell
    kubectl create secret generic mail-credentials \
    --from-literal=fromEmail=CHANGEME \
    --from-literal=host=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=password=CHANGEME \
    --from-literal=tls=CHANGEME \
    --from-literal=ssl=CHANGEME \
    -n webserver
    ```

5. Update [values.yaml](./values.yaml).

6. Deploy the helm chart.

    ```shell
    helm upgrade --install -n webserver webserver-staging .
    helm upgrade --install -n webserver webserver-main .
    ```

## Set up backup

The backup depends on a existing [restic](https://restic.readthedocs.io) repository available via sftp.

1. Initialize a restic repository.

    ```shell
    restic -r sftp:<ssh-alias>:<path-to-repository> init
    ```

2. Create a secret for the repository crendentials.

    ```shell
    kubectl create secret generic backup-restic-credentials \
    --from-literal=password=CHANGEME \
    -n webserver
    ```

3. Create a secret for the SSH Key.

    ```shell
    ssh-keyscan -t rsa,ecdsa,ed25519 -H -p CHANGEME CHANGEME > /tmp/known_hosts
    ```

    ```shell
    kubectl create secret generic backup-ssh-credentials \
    --from-literal=hostname=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=user=CHANGEME \
    --from-literal=path=CHANGEME \
    --from-file=knownHosts=/tmp/known_hosts \
    --from-file=privateKey=CHANGEME \
    -n webserver
    ```

4. Create a secret for mail credentials.
    For comprehensive documentation, refer to [python-sendmail](https://gitlab.com/mathezirkel/server/python-sendmail).

    ```shell
    kubectl create secret generic backup-mail-credentials \
    --from-literal=host=CHANGEME \
    --from-literal=port=CHANGEME \
    --from-literal=encryptionMode=CHANGEME \
    --from-literal=username=CHANGEME \
    --from-literal=password=CHANGEME \
    --from-literal=from=CHANGEME \
    --from-literal=to=CHANGEME \
    -n hedgedoc
    ```

5. Test your backup regularly! Manually create a backup by running

    ```shell
    argo submit --from workflowtemplate/webserver-main-backup -n webserver
    ```

## Adding basicAuth

1. Create a base64-encoded htpasswd string.

    ```shell
    htpasswd -n <username> | openssl base64
    ```

2. Adjust and apply the manifest `./manifest-templates/basic-auth/basicAuthSecret.yaml`:

    ```shell
    kubectl apply -f ./manifest-templates/basic-auth/basicAuthSecret.yaml
    ```

3. Add an item in `.Values.basicAuth`.

Comment: For some reason the basicAuth middleware does not work if the secret is directly applied via shell.

## Zirkel configuration

Adjust and apply the manifest `./templates/zirkel/zirkel-config.yaml`:

```shell
kubectl apply -f ./manifest-templates/zirkel/zirkel-config.yaml
```

## Values

### Deployment Backend

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| backend.config.debug | int | `0` | Enable debug mode |
| backend.config.djangoKey.existingSecret.key | string | `"secret"` | Key containing the django secret key |
| backend.config.djangoKey.existingSecret.name | string | `"django-secret-key"` | Name of an existing secret containing the django secret key |
| backend.config.mailCredentials.existingSecret.fromKey | string | `"fromEmail"` | Key containing the default email to send from |
| backend.config.mailCredentials.existingSecret.hostKey | string | `"host"` | Key containing the host |
| backend.config.mailCredentials.existingSecret.name | string | `"mail-credentials"` | Name of an existing secret containing the mail credentials |
| backend.config.mailCredentials.existingSecret.passwordKey | string | `"password"` | Key containing the password |
| backend.config.mailCredentials.existingSecret.portKey | string | `"port"` | Key containing the port |
| backend.config.mailCredentials.existingSecret.sslKey | string | `"ssl"` | Key containing the ssl settings |
| backend.config.mailCredentials.existingSecret.tlsKey | string | `"tls"` | Key containing the tls settings |
| backend.config.mailCredentials.existingSecret.userKey | string | `"user"` | Key containing the user |
| backend.config.mailMathezirkel | string | `"mathezirkel@math.uni-augsburg.de"` | The default email address receiving mails sent by the backend |
| backend.config.production | int | `1` | Enable production mode The mode assumes django behind a SSL terminating reverse proxy. |
| backend.config.ratelimit | string | `"1/s 3/m 5/10m 20/h 20/d 30/20d"` | Ratelimit for forms. See https://django-ratelimit.readthedocs.io/en/stable/usage.html for the syntax. Multiple rate limits can be separated by spaces. |
| backend.enabled | bool | `true` | Enable the backend |
| backend.image.pullPolicy | string | `"Always"` | Backend image pull policy |
| backend.image.repository | string | `"registry.gitlab.com/mathezirkel/server/website/backend"` | Backend image repository |
| backend.image.tag | string | `"main"` | Backend image tag |
| backend.livenessProbe.enabled | bool | `true` | Enable livenessProbe on backend containers |
| backend.livenessProbe.failureThreshold | int | `6` | Failure threshold for livenessProbe |
| backend.livenessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for livenessProbe |
| backend.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| backend.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| backend.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| backend.readinessProbe.enabled | bool | `true` | Enable readinessProbe on backend containers |
| backend.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| backend.readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| backend.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| backend.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| backend.readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for readinessProbe |
| backend.replicaCount | int | `1` | Number of backend replicas to deploy |
| backend.resources.limits.memory | string | `"256Mi"` | The memory limits for the backend containers |
| backend.resources.requests.cpu | string | `"100m"` | The requested cpu for the backend containers |
| backend.resources.requests.memory | string | `"64Mi"` | The requested memory for the backend containers |
| backend.service.port | int | `8000` | Backend service port |
| backend.startupProbe.enabled | bool | `true` | Enable startupProbe on backend containers |
| backend.startupProbe.failureThreshold | int | `6` | Failure threshold for startupProbe |
| backend.startupProbe.initialDelaySeconds | int | `10` | Initial delay seconds for startupProbe |
| backend.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| backend.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| backend.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| backend.strategy | object | `{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0},"type":"RollingUpdate"}` | Update Strategy of the Backend pods |
| backend.strategy.rollingUpdate.maxSurge | int | `1` | Maximum number of Pods that can be created over the desired number of Pods |
| backend.strategy.rollingUpdate.maxUnavailable | int | `0` | Maximum number of Pods that can be unavailable during the update process |
| backend.strategy.type | string | `"RollingUpdate"` | The strategy used to replace old Pods |

### Backup

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| backup.cleanUpPolicy.keepDaily | int | `7` | Number of recent daily snapshots to keep |
| backup.cleanUpPolicy.keepLast | int | `3` | Number of recent snapshots to keep |
| backup.cleanUpPolicy.keepMonthly | int | `6` | Number of recent monthly snapshots to keep |
| backup.cleanUpPolicy.keepWeekly | int | `4` | Number of recent weekly snapshots to keep |
| backup.enabled | bool | `true` | Enable the backup. |
| backup.failedJobsHistoryLimit | int | `2` | Number of failed jobs kept in history |
| backup.lastSuccessfulBackupThreshold | int | `24` | Maximum allowable age in hours for the most recent successful backup |
| backup.notification.existingSecret.encryptionModeKey | string | `"encryptionMode"` | Key containing the encryption method (Possible values: `ssl` or `starttls`) |
| backup.notification.existingSecret.fromKey | string | `"from"` | Key containing the email address sending the notification |
| backup.notification.existingSecret.hostKey | string | `"host"` | Key containing the hostname of the SMTP server |
| backup.notification.existingSecret.name | string | `"backup-mail-credentials"` | Name of an existing secret containing mail credentials for notification |
| backup.notification.existingSecret.passwordKey | string | `"password"` | Key containing the password for authentication |
| backup.notification.existingSecret.portKey | string | `"port"` | Key containing the SMTP port |
| backup.notification.existingSecret.toKey | string | `"to"` | Key containing an email address to receive backup notification |
| backup.notification.existingSecret.usernameKey | string | `"username"` | Key containing username for authentication |
| backup.pullPolicy | string | `"IfNotPresent"` | Workflow image pull policy |
| backup.restic.credentials.repository | object | `{"key":"password","name":"backup-restic-credentials"}` | Existing secret containing the repository credentials |
| backup.restic.credentials.repository.key | string | `"password"` | Key containing the password |
| backup.restic.credentials.repository.name | string | `"backup-restic-credentials"` | Name of the secret |
| backup.restic.credentials.ssh | object | `{"hostnameKey":"hostname","knownHostsKey":"knownHosts","name":"backup-ssh-credentials","pathKey":"path","portKey":"port","privateKeyKey":"privateKey","userKey":"user"}` | Existing secret containing the SSH credentials of the backup server |
| backup.restic.credentials.ssh.hostnameKey | string | `"hostname"` | Key containing the hostname |
| backup.restic.credentials.ssh.knownHostsKey | string | `"knownHosts"` | Key containing the known_hosts file |
| backup.restic.credentials.ssh.name | string | `"backup-ssh-credentials"` | Name of the secret |
| backup.restic.credentials.ssh.pathKey | string | `"path"` | Key containing the path |
| backup.restic.credentials.ssh.portKey | string | `"port"` | Key containing the port |
| backup.restic.credentials.ssh.privateKeyKey | string | `"privateKey"` | Key containing the private SSH key |
| backup.restic.credentials.ssh.userKey | string | `"user"` | Key containing the user |
| backup.restic.image.repository | string | `"restic/restic"` | Restic image repository |
| backup.restic.image.tag | string | `"0.17.0"` | Restic image tag Same as version on backup server |
| backup.schedule.backup | string | `"30 2 * * *"` | Schedule of the backup |
| backup.schedule.check | string | `"30 10 * * *"` | Schedule of the backup check |
| backup.schedule.clean | string | `"30 18 * * 0"` | Schedule of the backup clean up |
| backup.successfulJobsHistoryLimit | int | `1` | Number of successful jobs kept in history |

### Content

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| basicAuth | list | `[]` | List of tuples consisting of * name: Name of an existing secret of type basicAuth * paths: List of protected paths |
| persistentVolume.enabled | bool | `true` | Enable persistent volume |
| persistentVolume.mounts | list | `[]` | List of tuples consisting of * subPath: Path inside the nodePath containing the data * mountPath: location to mount this subPath |
| persistentVolume.nodeName | string | `"gernet"` | Hostname of the server containing the persistent volume |
| persistentVolume.nodePath | string | `"/srv/html/"` | Path containing the local website content |
| persistentVolume.size | string | `"200Gi"` | Size of the volume to create |

### Deployment

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `""` | String to fully override webserver.fullname |
| nameOverride | string | `""` | String to partially override webserver.fullname |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for the Nexcloud application If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | webserver.fullname | The name of the service account to use. |

### Ingress

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.additionalMiddlewares | list | `[{"name":"security-headers","namespace":"kube-public"}]` | List of additional middlewares to apply on the ingress |
| ingress.enabled | bool | `true` | Enable ingress |
| ingress.tls | object | `{"certResolver":"lets-encrypt"}` | SSL settings |

### Deployment NGINX

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| nginx.config.trustedProxies | string | `"10.42.0.0/24"` | Trusted proxy subnet |
| nginx.config.useDefaultConfig | bool | `false` | Use the default config of this chart |
| nginx.image.pullPolicy | string | `"Always"` | NGINX image pull policy |
| nginx.image.repository | string | `"registry.gitlab.com/mathezirkel/server/website/nginx"` | NGINX image repository |
| nginx.image.tag | string | `"main"` | NGINX image tag |
| nginx.livenessProbe.enabled | bool | `true` | Enable livenessProbe on NGINX containers |
| nginx.livenessProbe.failureThreshold | int | `6` | Failure threshold for livenessProbe |
| nginx.livenessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for livenessProbe |
| nginx.livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| nginx.livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| nginx.livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nginx.readinessProbe.enabled | bool | `true` | Enable readinessProbe on NGINX containers |
| nginx.readinessProbe.failureThreshold | int | `3` | Failure threshold for readinessProbe |
| nginx.readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| nginx.readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| nginx.readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| nginx.readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for readinessProbe |
| nginx.replicaCount | int | `1` | Number of NGINX replicas to deploy |
| nginx.resources.limits.memory | string | `"256Mi"` | The memory limits for the NGINX containers |
| nginx.resources.requests.cpu | string | `"100m"` | The requested cpu for the NGINX containers |
| nginx.resources.requests.memory | string | `"64Mi"` | The requested memory for the NGINX containers |
| nginx.service.port | int | `80` | NGINX service port |
| nginx.startupProbe.enabled | bool | `true` | Enable startupProbe on NGINX containers |
| nginx.startupProbe.failureThreshold | int | `6` | Failure threshold for startupProbe |
| nginx.startupProbe.initialDelaySeconds | int | `10` | Initial delay seconds for startupProbe |
| nginx.startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| nginx.startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| nginx.startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| nginx.strategy | object | `{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0},"type":"RollingUpdate"}` | Update Strategy of the NGINX pods |
| nginx.strategy.rollingUpdate.maxSurge | int | `1` | Maximum number of Pods that can be created over the desired number of Pods |
| nginx.strategy.rollingUpdate.maxUnavailable | int | `0` | Maximum number of Pods that can be unavailable during the update process |
| nginx.strategy.type | string | `"RollingUpdate"` | The strategy used to replace old Pods |

### Other Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.hosts.aliases[0] | string | `"mathezirkel-augsburg.de"` |  |
| ingress.hosts.host | string | `"www.mathezirkel-augsburg.de"` | Domain of the website |

## License

This project is licenced under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](./LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
