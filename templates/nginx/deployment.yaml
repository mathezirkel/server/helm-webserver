apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ printf "%s-nginx" (include "webserver.fullname" .) }}
  namespace: {{ .Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ printf "%s-nginx" (include "webserver.fullname" .) }}
    app.kubernetes.io/component: nginx
    {{- include "webserver.labels" . | nindent 4 }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ printf "%s-nginx" (include "webserver.fullname" .) }}
      app.kubernetes.io/component: nginx
      {{- include "webserver.selectorLabels" . | nindent 6 }}
  replicas: {{ .Values.nginx.replicaCount }}
  strategy:
    {{- with .Values.nginx.strategy }}
        {{- toYaml . | nindent 4 }}
    {{- end }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ printf "%s-nginx" (include "webserver.fullname" .) }}
        app.kubernetes.io/component: nginx
        {{- include "webserver.labels" . | nindent 8 }}
    spec:
      serviceAccountName: {{ include "webserver.serviceAccountName" . }}
      containers:
        - name: {{ printf "%s-nginx" (include "webserver.fullname" .) }}
          image: "{{ .Values.nginx.image.repository }}:{{ .Values.nginx.image.tag }}"
          imagePullPolicy: {{ .Values.nginx.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          env:
            - name: TRUSTED_PROXY
              value: {{ .Values.nginx.config.trustedProxies | quote }}
            {{- if .Values.backend.enabled }}
            - name: BACKEND_SERVICE
              value: {{ printf "%s-backend" (include "webserver.fullname" .) }}
            {{- end }}
          volumeMounts: {{ if or (and .Values.persistentVolume.enabled .Values.persistentVolume.mounts) .Values.nginx.config.useDefaultConfig }}
            {{ if .Values.persistentVolume.enabled }}
            {{- range .Values.persistentVolume.mounts }}
            - name: assets
              mountPath: {{ .mountPath }}
              subPath: {{ .subPath }}
              readOnly: true
            {{- end }}
            {{- end }}
            {{- if .Values.nginx.config.useDefaultConfig }}
            - name: nginx-config
              mountPath: /etc/nginx/templates/default.conf.template
              subPath: default.conf.template
            {{- end }}
            {{- else -}} []
            {{- end }}
          resources:
            {{- toYaml .Values.nginx.resources | nindent 12 }}
          {{- if .Values.nginx.startupProbe.enabled }}
          startupProbe:
            {{- omit .Values.nginx.startupProbe "enabled" | toYaml | nindent 12 }}
            httpGet:
              path: /healthy
              scheme: HTTP
              port: http
          {{- end }}
          {{- if .Values.nginx.readinessProbe.enabled }}
          readinessProbe:
            {{- omit .Values.nginx.readinessProbe "enabled" | toYaml | nindent 12 }}
            httpGet:
              path: /healthy
              scheme: HTTP
              port: http
          {{- end }}
          {{- if .Values.nginx.livenessProbe.enabled }}
          livenessProbe:
            {{- omit .Values.nginx.livenessProbe "enabled" | toYaml | nindent 12 }}
            httpGet:
              path: /healthy
              scheme: HTTP
              port: http
          {{- end }}
      volumes:
        {{- if .Values.persistentVolume.enabled }}
        - name: assets
          persistentVolumeClaim:
            claimName: {{ printf "%s-pvc" (include "webserver.fullname" .) }}
        {{- end }}
        {{- if .Values.nginx.config.useDefaultConfig }}
        - name: nginx-config
          configMap:
            name: {{ printf "%s-nginx-config" (include "webserver.fullname" .) }}
        {{- end }}
